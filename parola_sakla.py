parolalar = [] #parolalar değişkenini tanımladık
while True:#sonsuz while dongusune soktuk
    print('Bir işlem seçin')#islem secimini alttaki 2 satırla birlikte istedik
    print('1- Parolaları Listele')
    print('2- Yeni Parola Kaydet')
    islem = input('Ne Yapmak İstiyorsun :')#kullanıcıdan islem numarası aldık islem adlı degiskene atadık
    if islem.isdigit():#islem adlı degisken rakam ise if kosulunun altına gir
        islem_int = int(islem)#islemi inte cevirerek islemİ_int adlı degiskene ata
        if islem_int not in [1, 2]:#islem 1 veya 2 degilse alttaki komutları çalıştır
            print('Hatalı işlem girişi')#console hatalı islemi bildir
            continue#alttaki fonksyonlari çalıştırma ve en baştan while dongusune basla
        if islem_int == 2:#islem_int 2 ise alttaki komutları calistir

            girdi_ismi = input('Bir girdi ismi ya da web sitesi adresi girin :')#inputtan alınacak degeri girdi_ismi adlı degiskene ata

            kullanici_adi = input('Kullanici Adi Girin :')#inputtan alinacak kullanici adini kullanici_adi degiskenine ata

            parola = input('Parola :')#inputtan alinan degerleri degiskenlere atar,alttaki 3 satır boyunca ayni islem yapilir
            parola2 = input('Parola Yeniden :')
            eposta = input('Kayitli E-posta :')
            gizlisorucevabi = input('Gizli Soru Cevabı :')
            if kullanici_adi.strip() == '':#kullanici_adi degsikenindeki bosluklari yok et ve degiskenin ici bossa alttaki komutu calistir
                print('kullanici_adi girmediz')
                continue#alttaki komutlari es gec ve while bastan baslat
            if parola.strip() == '':#parola degiskenindeki boslukları yok et ve degiskeini ici bossa alttaki islemi calistir
                print('parola girmediz')
                continue#alttaki islemleri es gec while'i bastan baslat
            if parola2.strip() == '':#parola2 degiskenindeki boslukları yok et ve degiskeini ici bossa alttaki islemi calistir
                print('parola2 girmediz')
                continue#alttaki islemleri es gec while'i bastan baslat
            if eposta.strip() == '':#eposta degiskenindeki boslukları yok et ve degiskeini ici bossa alttaki islemi calistir
                print('eposta girmediz')
                continue#alttaki islemleri es gec while'i bastan baslat
            if gizlisorucevabi.strip() == '':#degiskendeki boslukları yok et ve degiskeini ici bossa alttaki islemi calistir
                print('gizlisorucevabi girmediz')
                continue#alttaki islemleri es gec while'i bastan baslat
            if girdi_ismi.strip() == '':#degiskendeki boslukları yok et ve degiskeini ici bossa alttaki islemi calistir
                print('Girdi ismi girmediz')
                continue#alttaki islemleri es gec while'i bastan baslat
            if parola2 != parola:#degiskendeki boslukları yok et ve degiskeini ici bossa alttaki islemi calistir
                print('Parolalar eşit değil')
                continue#alttaki islemleri es gec while'i bastan baslat

            yeni_girdi = {#alinan girdileri yeni_girdi adli sozluge ata
                'girdi_ismi': girdi_ismi,#inputtan ve girdi_ismi adli degiskene atanan degeri ayni isimle sozluge kaydet
                'kullanici_adi': kullanici_adi,#inputtan ve kullanici_adi adli degiskene atanan degeri ayni isimle sozluge kaydet
                'parola': parola,#inputtan ve parola adli degiskene atanan degeri ayni isimle sozluge kaydet
                'eposta': eposta,#inputtan ve girdi_epostadegiskene atanan degeri ayni isimle sozluge kaydet
                'gizlisorucevabi': gizlisorucevabi,#inputtan ve eposta adli degiskene atanan degeri ayni isimle sozluge kaydet
            }
            parolalar.append(yeni_girdi)#parolalar adli degiskene yeni_girdi adli sozlugun sonuna ekle
            continue#alttaki islemleri es gec while'i bastan baslat
        elif islem_int == 1:#eger islem 2 degil ve 1 ise alttaki komutlari calistir
            alt_islem_parola_no = 0#degiskenş tanimla ve icine 0 degerini ata
            for parola in parolalar:#en ustte tanimladigimiz parolalar adli dizinin icinde for dongusu ile dolas
                alt_islem_parola_no += 1#her donusunde ilgili variable'i 1 arttir
                print('{parola_no} - {girdi}'.format(parola_no=alt_islem_parola_no, girdi=parola.get('girdi_ismi')))#alt_islem_parola_no degiskenini parola no'ya ata,parola plarak tanimladigimiz dizi
            #icinde donen degerden girdi_ismi ni cek girdi adli degiskene ata ve iki degiskeni de yazdir
            alt_islem = input('Yukarıdakilerden hangisi ?: ')#sozlukteki hangi parolanin listenecegini sec degsikene at
            if alt_islem.isdigit(): #degisken rakamsa alttaki komutları calistir
                if int(alt_islem) < 1 and len(parolalar) - 1 < int(alt_islem):#alt_islem dedigimiz listenecek degisken nosu 'den kucuk oldugu ve parola sayisindan az oldugu takdirde hata yazisini yazdir
                    print('Hatalı parola seçimi')
                    continue#alttaki fonksyonlari çalıştırma ve en baştan while dongusune basla
                parola = parolalar[int(alt_islem) - 1]#altislem-1 kadar indisli olan parolalar adli diziyi parola degiskenine ata
                print('{kullanici}\n{parola}\n{gizli}\n{eposta}'.format( #parola adli for icinde donen degiskenden kullanici_adi degiskenini cekip kullanici'ya ata yazdir
                    kullanici=parola.get('kullanici_adi'),
                    parola=parola.get('parola'),#parola adli for icinde donen degiskenden parola degiskenini cekip parola'ya ata yazdir
                    eposta=parola.get('eposta'),#parola adli for icinde donen degiskenden eposta degiskenini cekip eposta'ya ata yazdir
                    gizli=parola.get('gizlisorucevabi'),#parola adli for icinde donen degiskenden gizlisorucevabi degiskenini cekip gizli'ya ata yazdir
                ))
                continue#alttaki islemleri es gec while'i bastan baslat

    print('Hatalı giriş yaptınız')
